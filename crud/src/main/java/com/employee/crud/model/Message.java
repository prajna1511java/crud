package com.employee.crud.model;

public class Message {

	private String messageType;
	
	private String code;
	private String description;
	public Message(String messageType, String code, String description) {
		super();
		this.messageType = messageType;
		this.code = code;
		this.description = description;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
