package com.employee.crud.validation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
public class SortByValidator{

	public boolean isValid(String value) {
		if(StringUtils.isNotBlank(value) && "".equalsIgnoreCase(value)) {
			return true;
		}
		return true;
	}
}
