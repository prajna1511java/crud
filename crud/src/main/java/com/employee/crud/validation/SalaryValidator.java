package com.employee.crud.validation;

import java.math.BigDecimal;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;

public class SalaryValidator implements ConstraintValidator<ValidSalary, String>{

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		// TODO Auto-generated method stub
		if(StringUtils.isBlank(value) ) {
			return false;
		}
		
		BigDecimal salary = new BigDecimal(value);
		if(salary.compareTo(BigDecimal.ZERO)<1) {
			return false;
		}
			return true;
		
	}

}
