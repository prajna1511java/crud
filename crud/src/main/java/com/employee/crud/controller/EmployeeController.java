package com.employee.crud.controller;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.employee.crud.constants.CommonConstants;
import com.employee.crud.dao.entity.Employee;
import com.employee.crud.model.EmployeeDetailsRequest;
import com.employee.crud.model.EmployeeDetailsResponse;
import com.employee.crud.service.EmployeeService;
	
@RestController
public class EmployeeController {

	private static final Logger logger = LoggerFactory.getLogger(EmployeeController.class);

	@Autowired
	private EmployeeService employeeService;
	
	
	/**
	 * 
	 * Get employee details based on EmployeeID
	 * @param id
	 * @return
	 */
	
	@RequestMapping(value ="/employee/get", method = RequestMethod.GET)
	public ResponseEntity<EmployeeDetailsResponse> getEmployeeDetailsById(@RequestParam String id) {
		EmployeeDetailsResponse response = new EmployeeDetailsResponse();
		try {
		
			return employeeService.getEmployeeDetailsById(id, response);
			 
		}catch (Exception e) {
			employeeService.buildResponse(response, CommonConstants.EXCEPTION, CommonConstants.EXCEPTION, CommonConstants.INTERNAL_SERVER_ERROR);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}
	}
	
	/**
	 * Fetch all employees based on EmployeeID
	 * @return
	 */
	@RequestMapping(value ="/employee/fetchall", method = RequestMethod.GET)
	public ResponseEntity<List<Employee>> fetchAllEmployees(@RequestParam(value = "pageNo" ,defaultValue = "0") Integer pageNo,
			@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
		List<Employee> employeeList = new ArrayList<Employee>();
		try {
			return employeeService.fetchAllEmployees(pageNo, pageSize,employeeList);
		}catch (Exception e) {
			// TODO: handle exception
			 return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(employeeList);
		}
	}
	
	/**
	 * Add new employee
	 * @param employeeDetails
	 * @param bindingResult
	 * @return
	 */
	
	@RequestMapping(value = "/employee/add", method = RequestMethod.POST)
	public ResponseEntity<EmployeeDetailsResponse> createEmployee(@RequestBody @Valid EmployeeDetailsRequest employeeDetails, BindingResult bindingResult) {
		EmployeeDetailsResponse response = new EmployeeDetailsResponse();
		try {
			logger.info(" Inside createEmployeeDetails method :");
			if(bindingResult.hasErrors()) {
				return employeeService.validationError(bindingResult, response);
			}
			
			return employeeService.createEmployee(employeeDetails, response);
		}catch (Exception e) {
			employeeService.buildResponse(response, CommonConstants.EXCEPTION, CommonConstants.EXCEPTION, CommonConstants.INTERNAL_SERVER_ERROR);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
			
		}
		
	}

	/**
	 * Update employee based on employeeId
	 * 
	 */
	
	@RequestMapping(value = "/employee/update/{id}", method = RequestMethod.PUT)
	public ResponseEntity<EmployeeDetailsResponse> updateEmployee(@RequestBody @Valid EmployeeDetailsRequest employeeDetails, BindingResult bindingResult, 
			@PathVariable(value = "id") BigInteger employeeId) {
		EmployeeDetailsResponse response = new EmployeeDetailsResponse();
		employeeDetails.setEmployeeId(employeeId);
		try {
			logger.info(" Inside createEmployeeDetails method :");
			if(bindingResult.hasErrors()) {
				return employeeService.validationError(bindingResult, response);
			}
				
			return employeeService.updateEmployee(employeeDetails, response);
			
		}catch (Exception e) {
			employeeService.buildResponse(response, CommonConstants.EXCEPTION, CommonConstants.EXCEPTION,CommonConstants.INTERNAL_SERVER_ERROR);
			 return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
			
		}
		
	}
	
	
	@RequestMapping(value = "/employee/remove", method = RequestMethod.DELETE)
	public ResponseEntity<String> deleteEmployee(@RequestParam(value = "id") String employeeId) {
		
		try {
			return employeeService.deleteEmployee(employeeId);
					
		}catch (Exception e) {
			// TODO: handle exception
			 return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
		
	} 
	

	
	
	
	
}
