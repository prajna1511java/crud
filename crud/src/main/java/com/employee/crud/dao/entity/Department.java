package com.employee.crud.dao.entity;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "DEPARTMENTS")
public class Department {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DEPARTMENT_ID",length = 4)
	private BigInteger departmentId;
	
	@Column(name = "DEPARTMENT_NAME", length = 30, nullable = false)
	private String departmentName;
	
	
	@OneToMany(mappedBy = "department", cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JsonIgnore
	private List<Employee> employees;
	
	/*
	 * @OneToOne(fetch = FetchType.LAZY)
	 * 
	 * @JoinColumn(name = "PARENT_DEPARTMENT_ID") private Department
	 * parentDepartment;
	 */

	

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	
	public List<Employee> getEmployees() {
		if(employees== null) {
			employees = new ArrayList<Employee>();
		}
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	/*
	 * public Department getParentDepartment() { return parentDepartment; }
	 * 
	 * public void setParentDepartment(Department parentDepartment) {
	 * this.parentDepartment = parentDepartment; }
	 */
	
	
	
	
}
