package com.employee.crud.dao;


import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.employee.crud.dao.entity.Employee;


@Service
public class EmployeeDao {
	
	@Autowired
	private EmployeeRepository employeeRepository;
	

	
	public Employee findByEmployeeId(BigInteger id) {
		
		employeeRepository.findByEmployeeId(id);
		return employeeRepository.findByEmployeeId(id);
	}
	
	public Employee saveEmployee(Employee emp) {
		return employeeRepository.save(emp);
	}

	public void deleteEmployee(Employee emp) {
		 employeeRepository.delete(emp);
	}
	
	public List<Employee> fetchAllEmployees(Integer pageNo,Integer pageSize){
	
		
		  Pageable paging = PageRequest.of(pageNo,pageSize);
		  
		  Page<Employee> employeePage= employeeRepository.findAll(paging);
		  
		  	if(employeePage.hasContent()) {
		  		return employeePage.getContent();
		  	}
		  	return new  ArrayList<Employee>();
		
		 
	}
		
}
