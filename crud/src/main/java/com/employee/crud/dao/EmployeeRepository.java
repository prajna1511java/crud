package com.employee.crud.dao;


import java.math.BigInteger;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.employee.crud.dao.entity.Employee;



@Repository
public interface EmployeeRepository extends CrudRepository<Employee, BigInteger>,
JpaRepository<Employee, BigInteger> {

	 public Employee findByEmployeeId(BigInteger employeeId);
	 
	 public List<Employee> findAllByEmployeeId(BigInteger employeeId, Pageable pageable);
	 
}
