package com.employee.crud.dao;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.employee.crud.dao.entity.Department;

@Repository
public interface DepartmentRepository extends CrudRepository<Department, BigInteger>, JpaRepository<Department, BigInteger> {

}
