package com.employee.crud.constants;

import org.springframework.stereotype.Component;

@Component
public final class CommonConstants {

	public static final String ERROR = "Error";
	public static final String SUCCESS = "Success";
	
	public static final String EXCEPTION = "Exception";
	public static final String DATE_FORMAT_YYYY_MM_DD ="yyyy-MM-dd";
	
	public static final String EMPLOYEE_ADDED = "EMPLOYEE_ADDED";
	public static final String EMPLOYEE_UPDATED="EMPLOYEE_UPDATED";
	public static final String EMPLOYEE_NOT_FOUND= "Employee not found";
	public static final String NOT_FOUND = "NOT_FOUND";
	public static final String EMPLOYEE_ADDED_SUCCESS="Employee sucessfully added";
	public static final String EMPLOYEE_UPDATED_SUCCESS="Employee sucessfully updated";
	public static final String EMPLOYEE_FOUND ="Employee found";
	public static final String FOUND = "FOUND";
	public static final String EMPLOYEE_ID_MANDATORY ="Emoylee Id is mandtory";
	public static final String INTERNAL_SERVER_ERROR = "Internal Server Error";
	
}
