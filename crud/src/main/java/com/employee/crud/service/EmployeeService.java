package com.employee.crud.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import com.employee.crud.constants.CommonConstants;
import com.employee.crud.dao.EmployeeDao;
import com.employee.crud.dao.entity.Department;
import com.employee.crud.dao.entity.Employee;
import com.employee.crud.model.EmployeeDetailsRequest;
import com.employee.crud.model.EmployeeDetailsResponse;
import com.employee.crud.model.Message;
import com.employee.crud.utility.CommonUtility;

@Service
public class EmployeeService {
	
	private static final Logger logger = LoggerFactory.getLogger(EmployeeService.class);
	
	@Autowired
	private EmployeeDao employeeDao;
	
	@Autowired
	private CommonUtility commonUtility;

	public ResponseEntity<EmployeeDetailsResponse> createEmployee(EmployeeDetailsRequest employeeDetails, EmployeeDetailsResponse response) throws Exception{
		Employee employee = new Employee();
		createEmployeeEntity(employeeDetails, employee);
		Department department = new Department();
		department.setDepartmentName(employeeDetails.getDepartment().getDepartmentName());
		employee.setDepartment(department); 
		Employee emp = employeeDao.saveEmployee(employee);
		response.setEmployee(emp);
		buildResponse(response,CommonConstants.SUCCESS,CommonConstants.EMPLOYEE_ADDED,CommonConstants.EMPLOYEE_ADDED_SUCCESS);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	private void createEmployeeEntity(EmployeeDetailsRequest employeeDetails, Employee employee) {
		employee.setFirstName(employeeDetails.getFirstName());
		employee.setLastName(employeeDetails.getLastName());
		employee.setHireDate(commonUtility.getDate(employeeDetails.getHireDate(), CommonConstants.DATE_FORMAT_YYYY_MM_DD));
		employee.setEmail(employeeDetails.getEmail());
		employee.setPhoneNumber(employeeDetails.getPhoneNumber());
		employee.setSalary(new BigDecimal(employeeDetails.getSalary()));
	}
	
	public ResponseEntity<EmployeeDetailsResponse> updateEmployee(EmployeeDetailsRequest employeeDetails, EmployeeDetailsResponse response) throws Exception{
		Employee employee = new Employee();
		employee = employeeDao.findByEmployeeId(employeeDetails.getEmployeeId());
		
		if(employee != null) {
			updateEmployeeEntity(employeeDetails, employee);
			response.setEmployee(employeeDao.saveEmployee(employee));
			buildResponse(response,CommonConstants.SUCCESS, CommonConstants.EMPLOYEE_UPDATED, CommonConstants.EMPLOYEE_UPDATED_SUCCESS);
			return ResponseEntity.status(HttpStatus.OK).body(response);
		}
		buildResponse(response, CommonConstants.ERROR, CommonConstants.NOT_FOUND, CommonConstants.EMPLOYEE_NOT_FOUND);
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
	}

	private void updateEmployeeEntity(EmployeeDetailsRequest employeeDetails, Employee employee) {
		employee.setFirstName(employeeDetails.getFirstName());
		employee.setLastName(employeeDetails.getLastName());
		employee.setHireDate((employeeDetails.getHireDate()!=null && !employeeDetails.getHireDate().isEmpty())
				? (commonUtility.getDate(employeeDetails.getHireDate(), CommonConstants.DATE_FORMAT_YYYY_MM_DD)) : employee.getHireDate());
		employee.setEmail((employeeDetails.getEmail()!=null &&  !employeeDetails.getEmail().isEmpty())
				? (employeeDetails.getEmail()): employee.getEmail());
		employee.setPhoneNumber((employeeDetails.getPhoneNumber()!=null && !employeeDetails.getPhoneNumber().isEmpty()) 
				? (employeeDetails.getPhoneNumber()): employee.getPhoneNumber());
		employee.setSalary(new BigDecimal(employeeDetails.getSalary()));
		Department department = employee.getDepartment()!=null ? employee.getDepartment() : new Department();
		
		if(StringUtils.isNotBlank(employeeDetails.getDepartment().getDepartmentName())) {
			
			department.setDepartmentName(employeeDetails.getDepartment().getDepartmentName());
			
		}else {
			department.setDepartmentName(employee.getDepartment().getDepartmentName());
		}
		employee.setDepartment(department);
	}
	
	
	public ResponseEntity<String> deleteEmployee(String employeeId) throws Exception{
		if(StringUtils.isNotBlank(employeeId)) {
			Employee emp = employeeDao.findByEmployeeId(new BigInteger(employeeId));
			if(emp!=null) {
				employeeDao.deleteEmployee(emp);	
				return ResponseEntity.status(HttpStatus.OK).body("Employee Id : "+employeeId+" is deleted");
			}
			return ResponseEntity.status(HttpStatus.OK).body(CommonConstants.EMPLOYEE_NOT_FOUND);
			
		}
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(CommonConstants.EMPLOYEE_ID_MANDATORY);
	}
	
	public ResponseEntity<EmployeeDetailsResponse> getEmployeeDetailsById(String id, EmployeeDetailsResponse response) throws Exception{
		if(StringUtils.isNotBlank(id)) {
			Employee emp =	employeeDao.findByEmployeeId(new BigInteger(id));
			if(emp != null) {
				response.setEmployee(emp);
				buildResponse(response, CommonConstants.SUCCESS, CommonConstants.FOUND, CommonConstants.EMPLOYEE_FOUND);
				return ResponseEntity.status(HttpStatus.OK).body(response);
			}
				buildResponse(response, CommonConstants.ERROR, CommonConstants.NOT_FOUND, CommonConstants.EMPLOYEE_NOT_FOUND);
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}else {
			buildResponse(response, CommonConstants.ERROR, CommonConstants.NOT_FOUND, CommonConstants.EMPLOYEE_ID_MANDATORY);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
	}
	
	public ResponseEntity<List<Employee>> fetchAllEmployees(Integer pageNo,Integer pageSize, List<Employee> employeeList) throws Exception{
		employeeList =  employeeDao.fetchAllEmployees(pageNo!=null ? pageNo : 0,pageSize!=null? pageSize : 10);
		 return ResponseEntity.status(HttpStatus.OK).body(employeeList);	
	}
	
	public void buildResponse(EmployeeDetailsResponse response, String messageType, String code, String message) {
		response.setTransactionStatus(messageType);
		Message msg = new Message(messageType,code,message);
		response.getMessages().add(msg);
	}
	
	public ResponseEntity<EmployeeDetailsResponse> validationError(BindingResult bindingResult,
			EmployeeDetailsResponse response) {
		logger.info(" Inside validationError method :");
		response.setTransactionStatus(CommonConstants.ERROR);
		List<ObjectError> errors = bindingResult.getAllErrors();
		for(ObjectError error : errors) {
			Message msg = new Message("ERROR", error.getCode(), error.getDefaultMessage());
			response.getMessages().add(msg);
		}
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
	}
}
